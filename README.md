# MobsToEggs - Public

No code for MobsToEggs is stored here, this repository is for the public facing wiki and issue reporting.

**Download MobsToEggs:**

- [SpigotMC](https://www.spigotmc.org/resources/69425/)

- [Modrinth](https://modrinth.com/plugin/mobstoeggs)

- [CurseForge](https://www.curseforge.com/minecraft/bukkit-plugins/mobstoeggs)

## WIKI
Find out how to use the plugin features at the [Wiki](https://gitlab.com/sugarfyi-team/public/mobstoeggs-public/-/wikis/home).

## ISSUES
Please report issues to the [issue tracker](https://gitlab.com/sugarfyi-team/public/mobstoeggs-public/-/issues).

You can also report issues on our [Discord](https://discord.gg/9nEFeG6zUH)
